/// State ///

// Result of an evaluation, could be an error
function Err(msg=null) {
  // Init
  this.msg = msg;
}

// Evaluation all ok
var OK = new Err("OK");


// Stack
function Stk(xs=[]) {
  // Init
  this.vals = xs;

  // Push value to stack
  this.push = (x) => this.vals.push(x);

  // Pop value from stack
  this.pop = (x) => this.vals.pop(x);
}

// Evaluation Context (Bindings)
function Ctx(p=null) {
  // Init
  this.parent = p;
  // bindings :: Map String Lst
  this.bindings = {};

  // Set Binding
  // set :: String -> Lst -> Null
  this.set = (k, v) => {
    this.bindings[k] = v.clone();
  };

  // Get Binding
  // get :: String -> Maybe Lst
  this.get = (k) => {
    if (k in this.bindings) return this.bindings[k].clone(); // Get from local
    if (this.parent != null) return this.parent.get(k); // Get from parent
    else return null; // No binding found
  }

  // Make new scope
  // newScope :: Ctx
  this.newScope = () => new Ctx(this);
}


/// AST ///

// Number Value
function Num(v) {
  // Init
  this.val = v;

  // Clone
  this.clone = () => new Num(this.val);

  // Eq
  // eq :: Expr -> Bool
  this.eq = (v) => {
    if (v instanceof Num) return v.val == this.val;
    return false;
  }

  // Evaluate
  // evaluate :: Stk -> Ctx -> Err
  this.evaluate = (stack, ctx) => {
    stack.push(this.clone());
    return OK;
  }
}

// Symbol Value
function Sym(v) {
  // Init
  this.val = v;

  // Clone
  this.clone = () => new Sym(this.val);

  // Eq
  // eq :: Expr -> Bool
  this.eq = (v) => {
    if (v instanceof Sym) return v.val == this.val;
    return false;
  }

  // Evaluate
  // evaluate :: Stk -> Ctx -> Msg
  this.evaluate = (stack, ctx) => {
    // If BIF call BIF
    if (this.val in BIF) return BIF[this.val](stack, ctx);
    // Otherwise, try to find its binding
    var b = ctx.get(this.val);
    // If we found a binding, call the associated program
    if (b != null) return b.call(stack, ctx);
    // If the symbol was unbound, return an error
    return new Err(`Binding ${this.val} not found`);
  }
}

// List Value
function Lst(vs) {
  // Init
  this.val = vs;

  // Clone
  this.clone = () => new Lst(this.val.map(x => x.clone()));

  // Eq
  // eq :: Expr -> Bool
  this.eq = (v) => {
    if (!(v instanceof Lst)) return false;
    if (v.val.length != this.val.length) return false;
    for (var i=0; i<this.val.length; i++) if (!(v.val[i].eq(this.val[i]))) return false;
    return true;
  }

  // Evaluate
  // evaluate :: Stk -> Ctx -> Err
  this.evaluate = (stack, ctx) => {
    stack.push(this.clone());
    return OK;
  }

  // Call this Lst as a program
  // call :: Stk -> Ctx -> Err
  this.call = (stack, ctx) => {
    var c = ctx.newScope();
    for (var expr of this.val) {
      var m = expr.evaluate(stack, c);
      if (m.msg != "OK") return m;
    }
    return OK;
  }
}

/// Parsing ///

function parse(x) {
  if (typeof x == 'number') return new Num(x);
  if (typeof x == 'string') return new Sym(x);
  if (x instanceof Array) return new Lst(x.map(parse));
  return null;
}

function unParse(e) {
  if (e instanceof Num || e instanceof Sym) return e.val;
  if (e instanceof Lst) return e.val.map(unParse);
  return null;
}



/// Built in Functions ///

// BIF :: Map String (Stk -> Ctx -> Err)
var BIF = {
  // Debug, print program state
  '.': (s, c) => {
    console.log('STACK: ', s);
    console.log('CTX: ', c);
    return OK;
  },

  // Add two numbers
  '+': (s, c) => {
    var x = s.pop();
    var y = s.pop();
    s.push(new Num(x.val + y.val));
    return OK;
  },

  // Check equality of two exprs
  '=': (s, c) => {
    var x = s.pop();
    var y = s.pop();
    s.push(new Num(x.eq(y) ? 1 : 0));
    return OK;
  },

  // Bind a symbol to a val
  'var': (s, c) => {
    var k = s.pop().val[0].val;
    var v = new Lst([s.pop()]);
    c.set(k, v);
    return OK;
  },

  // Define a function
  'def': (s, c) => {
    var k = s.pop().val[0].val;
    var v = s.pop();
    c.set(k, v);
    return OK;
  },

  // Evaluate list against the current stack
  'call': (s, c) => {
    return s.pop().call(s, c);
  },

  // If-else branch
  'if-else': (s, c) => {
    var falseProg = s.pop();
    var trueProg = s.pop();
    var cond = s.pop().val;
    if (cond != 0) return trueProg.call(s, c);
    return falseProg.call(s, c);
  },

}


/// Evaluate Program ///

function RunProgram(prog) {
  var s = new Stk();
  var c = new Ctx();
  var p = parse(prog);
  var e = p.call(s, c);
  return { s: s, c: c, p: p, e: e };
}


/// TESTING ///

function Test() {
  var program = [
    [["x"], "var", "x", "x"], ["dup"], "def",
    [["x"], "var"], ["drop"], "def",
    [[0], [1], "if-else"], ["!"], "def",
    [
      ["f"], "def",
      ["c"], "def",
      ["c", ["f", "r"], [], "if-else"], ["r"], "def",
      "r"
    ], ["while"], "def",
    0, ["dup", 5, "=", "!"], [".", 1, "+"], "while"
 ];
  var result = RunProgram(program);
  console.log(result);
}
