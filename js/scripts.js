
/// AST ///

// Number Value
function Num(v) {
  // Init
  this.val = v;
}

// Symbol Value
function Sym(v) {
  // Init
  this.val = v;
}

// List Value
function Lst(vs) {
  // Init
  this.val = vs;
}


/// Parsing ///

function parse(x) {
  if (typeof x == 'number') return new Num(x);
  if (typeof x == 'string') return new Sym(x);
  if (x instanceof Array) return new Lst(x.map(parse));
  return null;
}

function unParse(e) {
  if (e instanceof Num || e instanceof Sym) return e.val;
  if (e instanceof Lst) return e.val.map(unParse);
  return null;
}


/// State ///

// Stack
function Stk(xs=[]) {
  // Init
  this.vals = xs;

  // Push value to stack
  this.push = (x) => this.vals.push(x);

  // Pop value from stack
  this.pop = (x) => this.vals.pop(x);
}

// Evaluation Context (Bindings)
function Ctx(p=null) {
  // Init
  this.parent = p;
  // bindings :: Map String [Expr]
  this.bindings = {};

  // Set Binding
  this.set = (k, v) => this.bindings[k] = v;

  // Get Binding
  this.get = (k) => {
    if (k in this.bindings) return this.bindings[k]; // Get from local
    if (this.parent != null) return this.parent.get(k); // Get from parent
    else return null; // No binding found
  }

  // Make new scope
  this.newScope = () => new Ctx(this);
}


/// Evaluation ///

// Result of an evaluation, could be an error
function Err(msg=null) {
  // Init
  this.msg = msg;
}

// Evaluation all ok
var OK = new Err();

// Evaluate a single expression of the language
// evalExpr :: Stk -> Ctx -> Expr -> Err
function evalExpr(stack, ctx, expr) {
  // If the expr is a literal, just push it to the stack
  if ((expr instanceof Num) || (expr instanceof Lst)) {
    stack.push(expr);
    return OK;
  }
  // If the expr is a symbol try to call the BIF associated with it
  if (expr.val in BIF) return BIF[expr.val](stack, ctx);
  // Otherwise, try to find its binding
  var b = ctx.get(expr.val);
  // If we found a binding, call the associated program
  if (b != null) return evalProg(stack, ctx.newScope(), b);
  // If the symbol was unbound, return an error
  return new Err(`Binding ${expr.val} not found`);
}

// Evaluate a single expression of the language
// evalExpr :: Stk -> Ctx -> [Expr] -> Err
function evalProg(stack, ctx, prog) {
  for (var expr of prog) {
    var e = evalExpr(stack, ctx, expr);
    if (e.msg != null) return e;
  }
  return OK;
}

// Built in Functions
// BIF :: Map String (Stk -> Ctx -> Err)
var BIF = {
  // Print whole stack
  '.': (s, c) => {
    console.log(s);
    return OK;
  },

  // Add two numbers
  '+': (s, c) => {
    var x = s.pop();
    var y = s.pop();
    s.push(new Num(x.val + y.val));
    return OK;
  },

  // Bind a symbol to a list
  'set': (s, c) => {
    var k = s.pop().val[0].val;
    var v = s.pop().val;
    c.set(k, v);
    return OK;
  },

  // Evaluate list against the current stack
  'call': (s, c) => {
    var p = s.pop().val;
    return evalProg(s, c.newScope(), p);
  },

  // If-else branch
  'if-else': (s, c) => {
    var falseProg = s.pop().val;
    var trueProg = s.pop().val;
    var cond = s.pop().val;
    if (cond != 0) return evalProg(s, c.newScope(), trueProg);
    return evalProg(s, c.newScope(), falseProg);
  },

}



/// TESTING ///

var prog = [
  [ ['x'], 'set' ], ['drop'], 'set',
  [ ['x'], 'set', 'x', 'x' ], ['dup'], 'set',
  [ 'dup', ['dup', -1, '+', 'iter'], ['dup', 'drop'], 'if-else' ], ['iter'], 'set',
  10, 'iter'
];

var s = new Stk();
var c = new Ctx();
var p = parse(prog).val;
var r = evalProg(s, c, p);

console.log(s.vals.map(unParse));
